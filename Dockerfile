FROM alpine

RUN apk upgrade && apk add curl jq

RUN addgroup -S curl_group && adduser -S curl_user -G curl_group
USER curl_user

CMD ["curl"]